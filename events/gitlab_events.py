import gitlab
from dotenv import dotenv_values
import os

config = {
    **dotenv_values(".env"),
    **dotenv_values(".env.secret")
}

if config['GITLAB_TOKEN'] == '':
    gl = gitlab.Gitlab(config['GITLAB_URL'], job_token=os.environ['CI_JOB_TOKEN'])
else:
    gl = gitlab.Gitlab(config['GITLAB_URL'], private_token=config['GITLAB_TOKEN'])


def handle_issue_event(data):
    action = data['object_attributes']['state']
    issue_iid = data['object_attributes']['iid']
    project_id = data['project']['id']

    project = gl.projects.get(project_id)
    if action == 'opened':
        project.issues.get(issue_iid).notes.create({'body': 'Thanks for opening this issue!'})
    elif action == 'close':
        project.issues.get(issue_iid).notes.create({'body': 'Thanks for closing this issue!'})


def handle_merge_request_event(data):
    action = data['object_attributes']['action']
    merge_request_iid = data['object_attributes']['iid']
    project_id = data['project']['id']

    project = gl.projects.get(project_id)

    if action == 'open':
        project.mergerequests.get(merge_request_iid).notes.create({'body': 'A new merge request has been opened.'})
    elif action == 'close':
        project.mergerequests.get(merge_request_iid).notes.create({'body': 'The merge request has been closed.'})
    elif action == 'merge':
        project.mergerequests.get(merge_request_iid).notes.create({'body': 'The merge request has been merged.'})
