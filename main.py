from fastapi import FastAPI, Request, HTTPException
import hmac
import hashlib
from typing import Callable, Dict
from dotenv import dotenv_values
from events.gitlab_events import handle_issue_event
from events.gitlab_events import handle_merge_request_event

app = FastAPI()

config = {
    **dotenv_values(".env"),
    **dotenv_values(".env.secret")
}


def verify_signature(payload, signature):
    mac = hmac.new(config['GITLAB_SECRET'].encode(), msg=payload, digestmod=hashlib.sha256)
    return hmac.compare_digest(mac.hexdigest(), signature)


event_handlers: Dict[str, Callable[[dict], None]] = {
    'Issue Hook': handle_issue_event,
    'Merge Request Hook': handle_merge_request_event,
}


@app.post("/webhook")
async def webhook(request: Request):
    payload = await request.body()
    signature = request.headers.get('X-Gitlab-Token')

    if signature != config['GITLAB_SECRET']:
        raise HTTPException(status_code=400, detail="Invalid token")

    event = request.headers.get('X-Gitlab-Event')
    data = await request.json()

    handler = event_handlers.get(event)
    if handler:
        handler(data)
    else:
        raise HTTPException(status_code=400, detail="Unhandled event type")

    return {"detail": "Success"}


if __name__ == '__main__':
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)
